import socket
import random
import time
import sensorProtocol

HOST = socket.gethostname()
PORT = 50007
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
print('Connected!')
sensor_protocol = sensorProtocol.SensorProtocol(2)
for index in range(5):
    temperature_significand = random.randint(-5000, 12000)
    humidity_significand = random.randint(0, 10000)
    print(f"""Sent 
    Temperature: {sensorProtocol.significand_to_float(temperature_significand)} degrees
    Humidity: {sensorProtocol.significand_to_float(humidity_significand)} %""")
    data_package = sensor_protocol.encode_package(temperature_significand, humidity_significand)
    s.sendall(data_package)
    time.sleep(5)
s.close()
