import pytest
import sensorProtocol


temperature_base64_mapping = [
        (4, -5000, "eOz//wAAAAA="),
        (4, 0, "AAAAAAAAAAA="),
        (4, 2732, "rAoAAAAAAAA="),
        (4, 12000, "4C4AAAAAAAA="),
        (2, -5000, "eOwAAA=="),
        (2, 2732, "rAoAAA=="),
        (2, 12000, "4C4AAA=="),
    ]

humidity_base64_mapping = [
        (4, 0, "AAAAAAAAAAA="),
        (4, 7644, "AAAAANwdAAA="),
        (4, 10000, "AAAAABAnAAA="),
        (2, 0, "AAAAAA=="),
        (2, 7644, "AADcHQ=="),
        (2, 10000, "AAAQJw=="),
    ]


@pytest.mark.parametrize(
    'bytes_per_value, expected_temp_significand, input_base64', temperature_base64_mapping
)
def test_decode_package_temperature(bytes_per_value, input_base64, expected_temp_significand):
    sensor_protocol = sensorProtocol.SensorProtocol(bytes_per_value)
    temperature_significand, _ = sensor_protocol.decode_package(input_base64)
    assert temperature_significand == expected_temp_significand


@pytest.mark.parametrize(
    'bytes_per_value, expected_humidity_significand, input_base64', humidity_base64_mapping
)
def test_decode_package_humidity(bytes_per_value, input_base64, expected_humidity_significand):
    sensor_protocol = sensorProtocol.SensorProtocol(bytes_per_value)
    _, humidity_significand = sensor_protocol.decode_package(input_base64)
    assert humidity_significand == expected_humidity_significand


@pytest.mark.parametrize(
    'bytes_per_value, input_temp_significand, expected_base64', temperature_base64_mapping)
def test_encode_package_temperature(bytes_per_value, input_temp_significand, expected_base64):
    sensor_protocol = sensorProtocol.SensorProtocol(bytes_per_value)
    base64_package = sensor_protocol.encode_package(input_temp_significand, int(0))
    assert base64_package.decode() == expected_base64


@pytest.mark.parametrize(
    'bytes_per_value, input_hum_significand, expected_base64', humidity_base64_mapping
)
def test_encode_package_humidity(bytes_per_value, input_hum_significand, expected_base64):
    sensor_protocol = sensorProtocol.SensorProtocol(bytes_per_value)
    base64_package = sensor_protocol.encode_package(int(0), input_hum_significand)
    assert base64_package.decode() == expected_base64
