import sensorProtocol

dataInBase64 = "NAkAALoLAAA="
(temp, hum) = sensorProtocol.decode_package(dataInBase64)

print(f"Temperature: {temp} degrees")
print(f"Humidity: {hum} %")
