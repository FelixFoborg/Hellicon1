import socket
import sensorProtocol
import time


def is_data_bad(data_base64):
    return False


HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
s.settimeout(1)
sensor_protocol = sensorProtocol.SensorProtocol(2)
connections = []
time_last_execution = time.time()
while 1:
    print("Time since last execution: ", (time.time() - time_last_execution))
    time_last_execution = time.time()
    try:
        conn_socket, address = s.accept()
        connections.append((conn_socket, address))
        print('Connected by', address)
    except TimeoutError:
        print("No new sensor found")
    for connection in connections:
        data_in_base64 = connection[0].recv(8)
        if data_in_base64:
            if is_data_bad(data_in_base64):
                connection[0].close()
                connections.remove(connection)
                print(f'Connection {connection[1]} sends bad data')
                break
            (temperature_significand, humidity_significand) = sensor_protocol.decode_package(data_in_base64)
            print(f"""Sensor with ip-address {connection[1]} sends
                Temperature: {sensorProtocol.significand_to_float(temperature_significand)} degrees
                Humidity: {sensorProtocol.significand_to_float(humidity_significand)} %""")
# conn.close()
