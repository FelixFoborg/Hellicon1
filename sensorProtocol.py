import base64


def significand_to_float(significand):
    return significand * 10 ** (-2)


class SensorProtocol:
    byteorder = 'little'
    is_twos_complement_used = True

    def __init__(self, bytes_per_value=4):
        self.bytes_per_value = bytes_per_value

    def bytes_to_int(self, data_in_bytes):
        decimal = int.from_bytes(data_in_bytes, byteorder=self.byteorder, signed=self.is_twos_complement_used)
        return decimal

    def decode_package(self, base64_string):
        data_in_bytes = base64.b64decode(base64_string)

        temperature_significand = self.bytes_to_int(data_in_bytes[0:self.bytes_per_value])
        humidity_significand = self.bytes_to_int(data_in_bytes[self.bytes_per_value:2*self.bytes_per_value])

        return temperature_significand, humidity_significand

    def encode_package(self, temperature_significand, humidity_significand):
        data_in_bytes = self.encode_wire_protocol(humidity_significand, temperature_significand)
        return base64.b64encode(data_in_bytes)

    def encode_wire_protocol(self, humidity_significand, temperature_significand):
        data_in_bytes = bytearray(temperature_significand.to_bytes(self.bytes_per_value, byteorder=self.byteorder,
                                                                   signed=self.is_twos_complement_used))
        data_in_bytes.extend(humidity_significand.to_bytes(self.bytes_per_value, byteorder=self.byteorder,
                                                           signed=self.is_twos_complement_used))
        return data_in_bytes
